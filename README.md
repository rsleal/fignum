﻿<h1 align="center">Fignum code exercise</h1>

<p align="center">I've implemented a controller in /api/fignum with a single post action that accepts an int array 
that returns a ordered array of the prime numbers.</p>
<p align="center">The array should be a valid Json array that can only contain numbers</p>

## Build the Project
In the project directory:
```shell
dotnet build
```

## Run the Project
To run in Development environment:
```shell
dotnet run
```

A swagger page will be available at: [Swagger](https://www.linkedin.com/in/ricardoleal/ "Swagger")

## Test the Project
To execute the Unit tests:
```shell
dotnet test
```

Or if you want to test the API directly with cURL:
```shell
curl -X POST "https://localhost:44305/api/Fignum" -H  "accept: text/plain" -H  "Content-Type: application/json" -d "[0,1,2,3,4,5,6,7,8,9,10]"
```

## Author
[Ricardo Leal](https://www.linkedin.com/in/ricardoleal/ "Ricardo Leal")