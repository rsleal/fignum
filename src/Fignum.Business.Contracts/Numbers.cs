﻿using System;
using System.Collections.Generic;

namespace Fignum.Business.Contracts
{
    public class Numbers : List<int>
    {
        public Numbers()
        {

        }

        public Numbers(List<int> numbers)
        {
            this.AddRange(numbers);
        }
    }
}
