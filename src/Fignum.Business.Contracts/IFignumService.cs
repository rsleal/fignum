﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Fignum.Business.Contracts
{
    public interface IFignumService
    {
        Task<Numbers> GetPrimeNumbers(Numbers numbers, CancellationToken ct);
    }
}
