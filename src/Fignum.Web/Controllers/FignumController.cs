﻿using Fignum.Business.Contracts;
using Fignum.Web.Contracts;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Fignum.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class FignumController : ControllerBase
    {
        private readonly ILogger<FignumController> _logger;
        private readonly IFignumService _fignumService;

        public FignumController(
            ILogger<FignumController> logger,
            IFignumService fignumService)
        {
            _logger = logger;
            _fignumService = fignumService;
        }

        /// <summary>
        /// A post method that gets a string in the body of the request and returns an array of ints that are prime numbers
        /// the string format should be numbers separated by spaces or commas 
        /// It ignores parts of the string that are not numbers
        /// </summary>
        /// <param name="input"></param>
        /// <param name="ct"></param>
        /// <returns>A list of prime numbers</returns>
        [HttpPost]
        public async Task<GetPrimeNumbersResultModel> GetPrimeNumbers([FromBody] GetPrimeNumbersModel model, CancellationToken ct)
        {
            var numbers = model.Adapt<Numbers>();
            var result = await _fignumService.GetPrimeNumbers(numbers, ct);
            return result.Adapt<GetPrimeNumbersResultModel>();
        }
    }
}
