﻿using Fignum.Business.Contracts;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Fignum.Business
{
    public class FignumService : IFignumService
    {
        private readonly ILogger<FignumService> _logger;

        public FignumService(ILogger<FignumService> logger)
        {
            _logger = logger;
        }

        public Task<Numbers> GetPrimeNumbers(Numbers numbers, CancellationToken ct)
        {
            return Task.Run(() =>
            {
                if (numbers == null || numbers.Count == 0)
                {
                    return numbers;
                }

                var primeNumbers = numbers
                    .Where(number => IsPrimeNumber(number))
                    .ToList();
                primeNumbers.Sort();
                return new Numbers(primeNumbers);
            }, ct);
        }

        /// <summary>
        /// Method that returns if a number is a prime number or not.
        /// In math, prime numbers are whole numbers greater than 1, that have only two factors – 1 and the number itself.
        /// Prime numbers are divisible only by the number 1 or itself.
        /// </summary>
        /// <param name="n"></param>
        /// <returns>a Boolean whether the param n is a prime number or not</returns>
        private bool IsPrimeNumber(long n)
        {
            if (n <= 1) return false; // if n is 0 or 1 then it is not a prime number
            if (n == 2) return true; // if n is 2 then it is a prime number
            if (n % 2 == 0) return false; // if n is a even number is not a prime 

            // we don't need to iterate to number n, we can iterate only to the square root of n
            // if n is not a prime it can be factored into two numbers, say n1 and n2 which can't never be greater than square root of n
            // in some cases the computation cost of always calculating square root of n is not worth it versus iterating until n, I should probably add a condition here
            var sqrt = Math.Ceiling(Math.Sqrt(n));
            for (int i = 3; i <= sqrt; ++i)
            {
                if (n % i == 0)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
