﻿using Fignum.Business;
using Fignum.Business.Contracts;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Fignum.Tests
{
    public static class ServiceCollectionBuilder
    {
        public static ServiceProvider Build()
        {
            var services = new ServiceCollection()
                .AddSingleton(new LoggerFactory())
                .AddLogging()
                .AddScoped<IFignumService, FignumService>();

            return services.BuildServiceProvider();
        }
    }
}
