﻿using Fignum.Business.Contracts;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Fignum.Tests
{
    public class FignumServiceTests : IClassFixture<ServiceFixture<IFignumService>>
    {
        private readonly ServiceFixture<IFignumService> _fixture;
        public FignumServiceTests(ServiceFixture<IFignumService> fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public async Task GetPrimeNumbers_Number0_ShouldNotBePrime()
        {
            var ct = CancellationToken.None;
            var req = new Numbers() { 0 };

            var result = await _fixture.Service.GetPrimeNumbers(req, ct);

            Assert.Equal(new Numbers() { }, result);
        }

        [Fact]
        public async Task GetPrimeNumbers_Number1_ShouldNotBePrime()
        {
            var ct = CancellationToken.None;
            var req = new Numbers() { 1 };

            var result = await _fixture.Service.GetPrimeNumbers(req, ct);

            Assert.Equal(new Numbers() { }, result);
        }

        [Fact]
        public async Task GetPrimeNumbers_Number2_ShouldBePrime()
        {
            var ct = CancellationToken.None;
            var req = new Numbers() { 2 };

            var result = await _fixture.Service.GetPrimeNumbers(req, ct);

            Assert.Equal(new Numbers() { 2 }, result);
        }

        [Fact]
        public async Task GetPrimeNumbers_Number3_ShouldBePrime()
        {
            var ct = CancellationToken.None;
            var req = new Numbers() { 3 };

            var result = await _fixture.Service.GetPrimeNumbers(req, ct);

            Assert.Equal(new Numbers() { 3 }, result);
        }

        [Fact]
        public async Task GetPrimeNumbers_Numbers1To10_FilterPrimeNumbers()
        {
            var ct = CancellationToken.None;
            var req = new Numbers() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

            var result = await _fixture.Service.GetPrimeNumbers(req, ct);

            Assert.Equal(new Numbers() { 2, 3, 5, 7 }, result);
        }


        [Fact]
        public async Task GetPrimeNumbers_Numbers1To10_SortPrimeNumbers()
        {
            var ct = CancellationToken.None;
            var req = new Numbers() { 7, 3, 5, 2 };

            var result = await _fixture.Service.GetPrimeNumbers(req, ct);

            Assert.Equal(new Numbers() { 2, 3, 5, 7 }, result);
        }

        [Fact]
        public async Task GetPrimeNumbers_Numbers1To10_FilterAndSortPrimeNumbers()
        {
            var ct = CancellationToken.None;
            var req = new Numbers() { 10, 2, 6, 9, 5, 3, 1, 4, 8, 7 };

            var result = await _fixture.Service.GetPrimeNumbers(req, ct);

            Assert.Equal(new Numbers() { 2, 3, 5, 7 }, result);
        }

    }
}
