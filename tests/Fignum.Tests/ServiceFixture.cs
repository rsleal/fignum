﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace Fignum.Tests
{
    public class ServiceFixture<T> : IDisposable
    {
        public IServiceProvider ServiceProvider;
        public T Service;

        public ServiceFixture()
        {
            ServiceProvider = ServiceCollectionBuilder.Build();

            Service = ServiceProvider.GetRequiredService<T>();
        }

        public void Dispose()
        {
            ServiceProvider = null;
            Service = default;
        }
    }
}
